int led = 13;
int recv = 0;
 
void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}
 
void loop()
{
  if (Serial.available() > 0) {
    recv = Serial.read();
    
    digitalWrite(led, HIGH);
    delay(100);
    digitalWrite(led, LOW);

    Serial.print("Received: ");
    Serial.println(recv);
  }
}
