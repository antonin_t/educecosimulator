package net.tailhandier.ees.data;

public class XYZBlock {
    private float x;
    private float y;
    private float z;

    public XYZBlock(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public String toString() {
        return x + "," + y + "," + z;
    }

    public static XYZBlock fromString(String s) {
        if (s.matches("/(.*),(.*),(.*)/gi")) {
            String[] data = s.split(",");
            return new XYZBlock(Float.parseFloat(data[0]),
                    Float.parseFloat(data[1]),
                    Float.parseFloat(data[2]));
        }
        return null;
    }
}
