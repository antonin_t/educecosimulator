package net.tailhandier.ees;

import gnu.io.*;
import net.tailhandier.ees.gui.GuiManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

import static net.tailhandier.ees.EES.log;

public class SerialClass implements SerialPortEventListener {
    private GuiManager gui;
    private SerialPort serialPort;
    /*private static final String PORT_NAMES[] = {
            "/dev/tty.usbserial-A9007UX1",
            "/dev/ttyUSB0",
            "/dev/ttyACM0",
            "/dev/bus/usb/001/004",
            "COM11",
            "COM3"
    };*/
    private static BufferedReader input;
    private static OutputStream output;
    private static final int TIME_OUT = 2000;
    private static final int DATA_RATE = 9600;

    public SerialClass(GuiManager gui) {
        this.gui = gui;
    }

    public void setPort(String port) {
        CommPortIdentifier portId = null;

        try {
            portId = CommPortIdentifier.getPortIdentifier(port);
        } catch (NoSuchPortException ignore) {
        }

        if (portId == null) {
            log("==> Could not find Arduino's board port.");
            return;
        }

        try {
            // open serial port, and use class name for the appName.
            serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            // open the streams
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = serialPort.getOutputStream();
            char ch = 1;
            output.write(ch);

            // add event listeners
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                String inputLine = input.readLine();
                System.out.println("[INPUT] ==> " + inputLine);
                this.gui.setText(inputLine);
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }

    }

    public static synchronized void writeData(String data) {
        try {
            output.write(data.getBytes());
            log("==> Sending data: " + data);
        } catch (Exception e) {
            log("==> Could not write to Arduino board");
        }
    }
}

