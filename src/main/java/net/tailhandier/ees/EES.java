package net.tailhandier.ees;

import gnu.io.CommPortIdentifier;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.tailhandier.ees.gui.GuiManager;

import java.util.Enumeration;

public class EES extends Application {
    private static EES instance;
    private SerialClass serial;
    private Scene scene;

    public static void main(String[] args) {
        instance = new EES();
        instance.init(args);
    }

    public void init(String[] args) {
        log("==> Starting...");
        log("==> Started!");
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {

        this.scene = null;

        final GuiManager gui = new GuiManager();

        this.serial = new SerialClass(gui);

        /*> CPM <*/
        GridPane gpane = new GridPane();
        gpane.setHgap(10);
        gpane.setVgap(10);
        ObservableList<String> ports = FXCollections.observableArrayList();

        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            if (currPortId != null) {
                ports.add(currPortId.getName());
            }
        }

        Label lbl = new Label("Veuillez choisir le port du controlleur Arduino :");
        lbl.setFont(new Font("Arial", 14));
        final ChoiceBox<String> cb = new ChoiceBox<>();
        cb.setItems(ports);
        Button btn = new Button("Valider");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String port = cb.getValue();
                if (port != null && !port.equals("")) {
                    stage.hide();
                    stage.setWidth(900);
                    stage.setHeight(600);
                    serial.setPort(port);
                    stage.setX(stage.getX() - 350);
                    stage.setY(stage.getY() - 250);
                    stage.setTitle("[EES] Choisir vos valeurs");
                    scene.setRoot(gui);
                    stage.show();
                }
            }
        });
        gpane.add(lbl, 0, 0);
        HBox hb = new HBox();
        hb.getChildren().addAll(cb, btn);
        hb.setAlignment(Pos.CENTER);
        gpane.add(hb, 0, 1);
        //gpane.add(btn, 1, 1);
        gpane.setAlignment(Pos.CENTER);


        /*> CPM <*/

        scene = new Scene(gpane, 300, 100);

        stage.setTitle("[EES] Choisir un port");
        stage.setScene(scene);
        stage.setResizable(false);

        stage.getIcons().add(new Image(EES.class.getResourceAsStream("/ees.png")));

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                log("==> Stopping...");
                Platform.exit();
                log("==> Stopped!");
                System.exit(0);
            }
        });

        stage.show();
    }

    public static EES get() {
        return instance;
    }

    public SerialClass getSerial() {
        return this.serial;
    }

    public static void log(String s) {
        System.out.println("[EES] " + s);
    }
}