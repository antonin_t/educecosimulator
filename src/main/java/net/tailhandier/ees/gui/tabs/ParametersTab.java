package net.tailhandier.ees.gui.tabs;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import net.tailhandier.ees.EES;
import net.tailhandier.ees.gui.GuiManager;

import static net.tailhandier.ees.EES.log;

public class ParametersTab extends Tab {
    public ParametersTab(GuiManager gui) {
        super(gui);
    }

    @Override
    Node build(GuiManager gui) {
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.TOP_CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10, 10, 10, 10));

        Label label = new Label("Paramètres");
        label.setFont(new Font("Arial", 20));
        pane.add(label, 0, 0, 1, 1);

        Label vehicleMassLabel = new Label("Masse du véhicule (m) :");
        pane.add(vehicleMassLabel, 0, 1);

        TextField vehicleMassField = new TextField();
        pane.add(vehicleMassField, 1, 1);
        gui.getFieldList().add(vehicleMassField);

        Label gravityLabel = new Label("Gravité G (m.s^(-2)) :");
        //gravityLabel.setStyle("-fx-font-size: .7em; -fx-line-height: .8em;");
        pane.add(gravityLabel, 3, 1);

        TextField gravityField = new TextField();
        pane.add(gravityField, 4, 1);
        gui.getFieldList().add(gravityField);

        Label cxLabel = new Label("cx :");
        pane.add(cxLabel, 0, 2);

        TextField cxField = new TextField();
        pane.add(cxField, 1, 2);
        gui.getFieldList().add(cxField);

        Label volumicMassLabel = new Label("Masse volumique de l'air (p) :");
        pane.add(volumicMassLabel, 3, 2);

        TextField volumicMassField = new TextField();
        pane.add(volumicMassField, 4, 2);
        gui.getFieldList().add(volumicMassField);

        Label coupleLabel = new Label("Maître-couple (m^2) :");
        pane.add(coupleLabel, 0, 3);

        TextField coupleField = new TextField();
        pane.add(coupleField, 1, 3);
        gui.getFieldList().add(coupleField);

        Label roadFrictionsLabel = new Label("Frottement de la route (mu) :");
        pane.add(roadFrictionsLabel, 3, 3);

        TextField roadFrictionsField = new TextField();
        pane.add(roadFrictionsField, 4, 3);
        gui.getFieldList().add(roadFrictionsField);

        Label windAngleLabel = new Label("Angle du vent :");
        pane.add(windAngleLabel, 0, 4);

        TextField windAngleField = new TextField();
        pane.add(windAngleField, 1, 4);
        gui.getFieldList().add(windAngleField);

        Label windSpeedLabel = new Label("Vitesse du vent (m.s^(-1)) :");
        pane.add(windSpeedLabel, 3, 4);

        TextField windSpeedField = new TextField();
        pane.add(windSpeedField, 4, 4);
        gui.getFieldList().add(windSpeedField);

        Label calculationStepLabel = new Label("Pas de calcul (s) :");
        pane.add(calculationStepLabel, 0, 5);

        TextField calculationStepField = new TextField();
        pane.add(calculationStepField, 1, 5);
        gui.getFieldList().add(calculationStepField);

        return pane;
    }
}
