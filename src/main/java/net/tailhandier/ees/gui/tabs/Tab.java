package net.tailhandier.ees.gui.tabs;

import javafx.scene.Node;
import net.tailhandier.ees.gui.GuiManager;

public abstract class Tab {
    private Node tab;

    Tab(GuiManager gui) {
        this.tab = build(gui);
    }

    public Node get() {
        return this.tab;
    }

    abstract Node build(GuiManager gui);
}
