package net.tailhandier.ees.gui.tabs;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import net.tailhandier.ees.data.XYZBlock;
import net.tailhandier.ees.gui.GuiManager;

import java.util.ArrayList;
import java.util.List;

public class ValuesTab extends Tab {
    public ValuesTab(GuiManager gui) {
        super(gui);
    }

    @Override
    Node build(final GuiManager gui) {
        final VBox vbox = new VBox();

        Label label = new Label("Valeurs");
        label.setFont(new Font("Arial", 20));

        final TableView table = new TableView();
        table.setEditable(false);
        Text tableplaceh = new Text("Aucune valeur n'a été renseignée.");
        tableplaceh.setFill(Color.DARKGRAY);
        table.setPlaceholder(tableplaceh);


        TableColumn xCol = new TableColumn("X");
        xCol.setCellValueFactory(new PropertyValueFactory<>("x"));
        TableColumn<Object, Object> yCol = new TableColumn<>("Y");
        yCol.setCellValueFactory(new PropertyValueFactory<>("y"));
        TableColumn zCol = new TableColumn("Z");
        zCol.setCellValueFactory(new PropertyValueFactory<>("z"));

        table.getColumns().addAll(xCol, yCol, zCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(gui.getData());
        for (Object tc_ : table.getColumns()) {
            TableColumn tc = (TableColumn) tc_;
            tc.setResizable(false);
            tc.setSortable(false);
            tc.setEditable(false);
            float size = 0.333333F;
            tc.prefWidthProperty().bind(table.widthProperty().multiply(size));
        }

        table.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.DELETE) || event.getCode().equals(KeyCode.BACK_SPACE)) {
                    XYZBlock currentBlock = (XYZBlock) table.getSelectionModel().getTableView().getItems().get(table.getSelectionModel().getSelectedIndex());
                    gui.getData().remove(currentBlock);
                }
            }
        });

        List<Control> fields = new ArrayList<>();

        final TextField xField = new TextField();
        xField.setPromptText("Coordonnée X");
        fields.add(xField);

        final TextField yField = new TextField();
        yField.setPromptText("Coordonnée Y");
        fields.add(yField);

        final TextField zField = new TextField();
        zField.setPromptText("Coordonnée Z");
        fields.add(zField);

        Button validate = new Button("Ajouter");
        fields.add(validate);

        GridPane actionresultgrid = new GridPane();
        actionresultgrid.setAlignment(Pos.TOP_CENTER);
        final Text actionresult = new Text();
        actionresult.setTextAlignment(TextAlignment.CENTER);
        actionresultgrid.add(actionresult, 0, 0);

        for (Control c : fields) {
            c.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode().equals(KeyCode.ENTER)) {
                        verify(xField, yField, zField, gui.getData(), actionresult);
                    }
                }
            });
        }
        validate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                verify(xField, yField, zField, gui.getData(), actionresult);
            }
        });

        GridPane buttons = new GridPane();
        buttons.setAlignment(Pos.TOP_CENTER);
        buttons.setHgap(10);
        buttons.setVgap(10);
        buttons.setPadding(new Insets(10, 10, 10, 10));
        buttons.add(xField, 0, 0);
        buttons.add(yField, 1, 0);
        buttons.add(zField, 2, 0);
        buttons.add(validate, 3, 0);
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll(label, table, buttons, actionresultgrid);

        return vbox;
    }

    private void verify(TextField xField, TextField yField, TextField zField, ObservableList<XYZBlock> data, Text actionresult) {
        if (data.size() >= 50) {
            actionresult.setFill(Color.FIREBRICK);
            actionresult.setText("Vous ne pouvez pas indiquer de valeur supplémentaire. (Limite = 50)");
            GuiManager.hideText(actionresult);
            return;
        }
        String x_ = xField.getText();
        String y_ = yField.getText();
        String z_ = zField.getText();
        if (x_ != null && !x_.equals("") && y_ != null && !y_.equals("") && z_ != null && !z_.equals("")) {
            try {
                x_ = x_.replaceAll(",", ".");
                y_ = y_.replaceAll(",", ".");
                z_ = z_.replaceAll(",", ".");
                float x = Float.parseFloat(x_);
                float y = Float.parseFloat(y_);
                float z = Float.parseFloat(z_);
                data.add(new XYZBlock(x, y, z));
                actionresult.setFill(Color.GREEN);
                actionresult.setText("Vos valeurs ont bien été ajoutées !");
                xField.requestFocus();
            } catch (NumberFormatException ignore) {
                actionresult.setFill(Color.FIREBRICK);
                actionresult.setText("Merci d'indiquer des valeurs correctes ! (nombres décimaux)");
            }
            xField.clear();
            yField.clear();
            zField.clear();
        } else {
            actionresult.setFill(Color.FIREBRICK);
            actionresult.setText("Merci d'indiquer toutes les valeurs !");
        }
        GuiManager.hideText(actionresult);
    }
}
