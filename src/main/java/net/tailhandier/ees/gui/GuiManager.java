package net.tailhandier.ees.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import net.tailhandier.ees.data.XYZBlock;
import net.tailhandier.ees.gui.tabs.ParametersTab;
import net.tailhandier.ees.gui.tabs.ValuesTab;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static net.tailhandier.ees.SerialClass.writeData;

public class GuiManager extends GridPane {
    private ParametersTab ptab;
    private ValuesTab vtab;
    private final ObservableList<XYZBlock> tabledata;
    private List<TextField> fieldlist;
    private Text result;

    public GuiManager() {
        tabledata = FXCollections.observableArrayList();
        fieldlist = new ArrayList<>();

        ptab = new ParametersTab(this);
        vtab = new ValuesTab(this);

        this.setAlignment(Pos.TOP_CENTER);
        this.setPadding(new Insets(0, 0, 10, 0));
        this.setVgap(0);
        this.setHgap(0);

        init();
    }

    private void init() {
        TabPane tabPane = new TabPane();

        Tab parametersTab = new Tab();
        parametersTab.setText("Paramètres");
        parametersTab.setContent(ptab.get());
        parametersTab.setClosable(false);

        Tab valuesTab = new Tab();
        valuesTab.setText("Valeurs");
        valuesTab.setContent(vtab.get());
        valuesTab.setClosable(false);

        tabPane.getTabs().add(parametersTab);
        tabPane.getTabs().add(valuesTab);


        result = new Text();
        result.setTextAlignment(TextAlignment.CENTER);

        HBox actionresultbox = new HBox();
        actionresultbox.setAlignment(Pos.CENTER);
        actionresultbox.getChildren().add(result);

        Button btn = new Button("Envoyer au controlleur Arduino");
        btn.setAlignment(Pos.CENTER);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                verify(result);
            }
        });

        HBox btnbox = new HBox();
        btnbox.setAlignment(Pos.CENTER);
        btnbox.setPadding(new Insets(0, 0, 10, 0));
        btnbox.getChildren().add(btn);

        this.add(tabPane, 0, 0);
        this.add(btnbox, 0, 1);
        this.add(actionresultbox, 0, 2);

        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(100);
        this.getColumnConstraints().add(column);
    }

    private void verify(final Text actionresult) {
        if (this.getData().size() <= 4) {
            actionresult.setFill(Color.FIREBRICK);
            actionresult.setText("Merci d'indiquer au minimum 5 valeurs dans le tableau des valeurs.");
            hideText(actionresult);
            return;
        }
        boolean ok = true;
        for (TextField tf : this.getFieldList()) {
            if (tf.getText() == null || tf.getText().equals("")) ok = false;
        }
        if (!ok) {
            actionresult.setFill(Color.FIREBRICK);
            actionresult.setText("Merci de bien vouloir remplir tous les paramètres.");
            hideText(actionresult);
            return;
        }
        actionresult.setFill(Color.GREEN);
        actionresult.setText("Vos valeurs ont bien été transmises au système Arduino !");
        List<String> result = new ArrayList<>();
        for (TextField tf : this.getFieldList()) {
            result.add(tf.getText());
        }
        List<String> data = new ArrayList<>();
        for (XYZBlock xyz : this.tabledata) {
            data.add(xyz.toString());
        }
        result.add(data.toString());
        writeData(result.toString());
        hideText(actionresult);
    }

    public static void hideText(final Text actionresult) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                actionresult.setText("");
            }
        }, 4000);
    }

    public ObservableList<XYZBlock> getData() {
        return this.tabledata;
    }

    public List<TextField> getFieldList() {
        return this.fieldlist;
    }

    public void setText(String t) {
        result.setText(t);
        hideText(result);
    }
}
